class GameField {
    mode;
    state;
	isOverGame;

    constructor(mode, state, isOverGame){
		this.mode = mode; //вызывается сеттер
        this.state = state;
        this.isOverGame = isOverGame;
    }

    set setMode(newMode) {
        this.mode = newMode;
    }

    set setState(newState) {
        this.state = newState;
    }

    set setIsOverGame (newIsOverGame) {
        this.isOverGame = newIsOverGame;
    }

    getGameFieldStatus () {
        //Диагональ
        if (state['field00'] === 'x' &&
            state['field11'] === 'x' &&
            state['field22'] === 'x'){
                this.setMode = "Выиграли x";
                this.setIsOverGame = true;
            }
        else if (state['field02'] === 'x' &&
            state['field11'] === 'x' &&
            state['field20'] === 'x'){
                this.setMode = "Выиграли x";
                this.setIsOverGame = true;
            }
        else if(state['field00'] === 'o' &&
            state['field11'] === 'o' &&
            state['field22'] === 'o'){
                this.setMode = "Выиграли o";
                this.setIsOverGame = true;
            }
        else if (state['field02'] === 'o' &&
            state['field11'] === 'o' &&
            state['field20'] === 'o'){
                this.setMode = "Выиграли o";
                this.setIsOverGame = true;
            }
        //Горизонталь
        if (state['field00'] === 'x' &&
            state['field01'] === 'x' &&
            state['field02'] === 'x'){
                this.setMode = "Выиграли x";
                this.setIsOverGame = true;
            }
        else if (state['field10'] === 'x' &&
            state['field11'] === 'x' &&
            state['field12'] === 'x'){
                this.setMode = "Выиграли x";
                this.setIsOverGame = true;
            }
        else if (state['field20'] === 'x' &&
            state['field21'] === 'x' &&
            state['field22'] === 'x'){
                this.setMode = "Выиграли x";
                this.setIsOverGame = true;
            }
        else if(state['field00'] === 'o' &&
            state['field01'] === 'o' &&
            state['field02'] === 'o'){
                this.setMode = "Выиграли o";
                this.setIsOverGame = true;
            }
        else if (state['field10'] === 'o' &&
            state['field11'] === 'o' &&
            state['field12'] === 'o'){
                this.setMode = "Выиграли o";
                this.setIsOverGame = true;
            }
        else if (state['field20'] === 'o' &&
            state['field21'] === 'o' &&
            state['field22'] === 'o'){
                this.setMode = "Выиграли o";
                this.setIsOverGame = true;
            }
        //Вертикаль
        if (state['field00'] === 'x' &&
            state['field10'] === 'x' &&
            state['field20'] === 'x'){
                this.setMode = "Выиграли x";
                this.setIsOverGame = true;
            }
        else if (state['field01'] === 'x' &&
            state['field11'] === 'x' &&
            state['field21'] === 'x'){
                this.setMode = "Выиграли x";
                this.setIsOverGame = true;
            }
        else if (state['field02'] === 'x' &&
            state['field12'] === 'x' &&
            state['field22'] === 'x'){
                this.setMode = "Выиграли x";
                this.setIsOverGame = true;
            }
        else if(state['field00'] === 'o' &&
            state['field10'] === 'o' &&
            state['field20'] === 'o'){
                this.setMode = "Выиграли o";
                this.setIsOverGame = true;
            }
        else if (state['field01'] === 'o' &&
            state['field11'] === 'o' &&
            state['field21'] === 'o'){
                this.setMode = "Выиграли o";
                this.setIsOverGame = true;
            }
        else if (state['field02'] === 'o' &&
            state['field12'] === 'o' &&
            state['field22'] === 'o')
            {
                this.setMode = "Выиграли o";
                this.setIsOverGame = true;
            }
        if(mode === 'x' || mode === 'o')
        {
            let check = true;
            for (const [key, value] of Object.entries(state)) {
                if(value === null)
                {
                    check = false;
                    break;
                }
            }
            if (check)
            {
                this.setMode = 'Ничья';
                this.setIsOverGame = true;
            }
        }
    }

    setFieldCellValue (value, mode) {
        let cell = 'field'+value
        if(state[cell] === null)
        {
            state[cell] = mode;
            if(mode === 'x')
                this.setMode = 'o'
            else  this.setMode = 'x'
        }
    }

}
const mode = 'x';
const state = {
    field00: null,
    field01: null,
    field02: null,
    field10: null,
    field11: null,
    field12: null,
    field20: null,
    field21: null,
    field22: null
    };
const isOverGame = false;
const gameField = new GameField(mode, state, isOverGame);
while (!gameField.isOverGame) {
    const response = prompt(`Ходят ${gameField.mode} (пример ввода: 00, 12, 22)`);
    gameField.setFieldCellValue(response, gameField.mode);
    gameField.getGameFieldStatus();
}
console.log(gameField.state);
alert(gameField.mode)
