import Header from '../Components/Header';
import React from "react";
import styles from "../Rating/rating.module.css";

function RatingPage() {
    const user = [
        { name: 'Александров Игнат Анатолиевич', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Шевченко Рафаил Михайлович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Мазайло Трофим Артёмович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Логинов Остин Данилович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Борисов Йошка Васильевич', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Соловьёв Ждан Михайлович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Негода Михаил Эдуардович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Гордеев Шамиль Леонидович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Волков Эрик Алексеевич', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Кузьмин Ростислав Васильевич', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Стрелков Филипп Борисович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
        { name: 'Галкин Феликс Платонович', game: "24534", win: 9824, lose: 1222, winrate: '87%'}
    ];
    return (
        <div>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
            <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet" />
            <Header />
            <div className={styles.maincontainer}>
                <div className={styles.tablecontainer}>
                    <div className={styles.subjectheader}>Рейтинг игроков</div>
                    <div iclassName={styles.subjecttable}>
                        <div className={styles.wrapper}>
                            <div className={styles.subjectrow}>
                                <div className={styles.tableheader}>ФИО</div>
                                <div className={styles.tableheader}>Всего игр</div>
                                <div className={styles.tableheader}>Победы</div>
                                <div className={styles.tableheader}>Проигрыши</div>
                                <div className={styles.tableheader}>Процент побед</div>
                            </div>
                            {user.map(item => (
                                <div className={styles.subjectrow}>
                                    <div className={styles.cell1}>{item.name}</div>
                                    <div className={styles.cell1}>{item.game}</div>
                                    <div className={styles.cell2}>{item.win}</div>
                                    <div className={styles.cell3}>{item.lose}</div>
                                    <div className={styles.cell1}>{item.winrate}</div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>




    );
}

export default RatingPage;