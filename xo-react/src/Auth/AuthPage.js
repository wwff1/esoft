import styles from "./auth.module.css";
import Field from '../Components/Field';
import { useNavigate } from "react-router-dom";


import React from 'react';

function AuthPage() {
    const navigate = useNavigate();
    const pressOnInput = (e) => {
        if (e.charCode == 46 || e.charCode == 95 || (e.charCode >= 65 && e.charCode <= 90)
            || (e.charCode >= 97 && e.charCode <= 122) || (e.charCode >= 48 && e.charCode <= 57))
            return true;
        else {
            e.preventDefault();
        }
    };


    return (
        <div className={styles.maincontainer}>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
            <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet" />
            <form className={styles.authcontainer} method="POST" action="/">
                <div><img className={styles.subjecticon} src="./auth-logo.svg"/></div>
                <div className={styles.subjectheader}>Войдите в игру</div>
                <div className={styles.authinteractiveelements}>
                    <Field placeholder="Логин" id="login" className={styles.subjecttext} onKeyPress={(e) => pressOnInput(e)}/>
                    <Field type="text" placeholder="Пароль" id="password" className={styles.subjecttext} onKeyPress={(e) => pressOnInput(e)}/>

                </div>
                <input type="submit" className={styles.subjectbutton} value="Войти" onClick={() => navigate("/gameField")}/>
            </form>
        </div>
    );
}

export default AuthPage;