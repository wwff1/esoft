import Header from '../Components/Header';
import React, {useState} from "react";
import styles from "../Active/active.module.css";

function AuthPage() {
    const [checked, setChecked] = useState(false);
    const user = [
        {id:0, name: 'Александров Игнат Анатолиевич', status: true},
        {id:1, name: 'Шевченко Рафаил Михайлович', status: false},
        {id:2, name: 'Мазайло Трофим Артёмович', status: true},
        {id:3, name: 'Логинов Остин Данилович', status: false},
        {id:4, name: 'Борисов Йошка Васильевич', status: false},
        {id:5, name: 'Соловьёв Ждан Михайлович', status: false},
        {id:6, name: 'Негода Михаил Эдуардович', status: false},
        {id:7, name: 'Гордеев Шамиль Леонидович', status: true},
        {id:8, name: 'Волков Эрик Алексеевич', status: false},
        {id:9, name: 'Кузьмин Ростислав Васильевич', status: false}
    ];


    return (
        <div>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
            <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet" />
            <Header />
            <div className={styles.maincontainer}>
                <div className={styles.tablecontainer}>
                    <div className={styles.subjectheader}>Активные игроки
                        <div className={styles.toogle}>Только свободные<input type="checkbox" id="switch" checked={checked}  onChange={() => setChecked(!checked)} /><label className={styles.label}
                            htmlFor="switch">Toggle</label></div>

                    </div>

                    <div className={styles.subjecttable}>
                        <div className={styles.wrapper}>
                            {user.map(item => (
                                <div>
                                {item.status === true &&
                                    <div className={styles.subjectrow}>
                                        <div className={styles.cell1}>{item.name}</div>
                                        <div className={styles.cell2Free}>Свободен</div>
                                        <div className={styles.cell3Free}>Позвать играть</div>
                                    </div>
                                }
                                    {item.status === false && !checked &&
                                    <div className={styles.subjectrow}>
                                        <div className={styles.cell1 }>{item.name}</div>
                                        <div className={styles.cell2}>В игре</div>
                                        <div className={styles.cell3}>Позвать играть</div>
                                    </div>
                                    }
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>




    );
}

export default AuthPage;