import Header from '../Components/Header';
import Row from '../Components/Row'
import React, {useState} from "react";
import styles from "../List/list.module.css";
import Modal from "../Components/ModalAdd";

function ListPage() {
    const [isOpen, setIsOpen] = useState(false);

    const user = [
        { name: "Александров Игнат Анатолиевич", age: "24", gender: false, status: true, create: "12 октября 2021", change: "22 октября 2021"},
        { name: "Мартынов Остап Фёдорович", age: "12", gender: false, status: false, create: "12 октября 2021", change: "22 октября 2021"},
        { name: "Комаров Цефас Александрович", age: "83", gender: true, status: true, create: "12 октября 2021", change: "22 октября 2021"},
        { name: "Кулаков Станислав Петрович", age: "43", gender: true, status: false, create: "12 октября 2021", change: "22 октября 2021"},
        { name: "Борисов Йошка Васильевич", age: "32", gender: false, status: true, create: "12 октября 2021", change: "22 октября 2021"},
        { name: "Негода Михаил Эдуардович", age: "33", gender: true, status: true, create: "12 октября 2021", change: "22 октября 2021"},
        { name: "Жданов Зураб Алексеевич", age: "24", gender: true, status: true, create: "12 октября 2021", change: "22 октября 2021"},
        { name: "Бобров Фёдор Викторович", age: "19", gender: true, status: false, create: "12 октября 2021", change: "22 октября 2021"},
        { name: "Многогрешный Павел Виталиевич", age: "24", gender: true, status: false, create: "12 октября 2021", change: "22 октября 2021"},
    ];

    return (
        <div>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
            <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet" />
            <Header />
            <div className={styles.maincontainer}>
                <div className={styles.tablecontainer}>
                    <div className={styles.subjectheader}>Список игроков
                        <div className={styles.add} onClick={() => setIsOpen(true)}>Добавить игрока</div>
                    </div>
                    <div className={styles.subjecttable}>
                        <div className={styles.wrapper}>
                            <div className={styles.subjectrow}>
                                <div className={styles.tableheader}>ФИО</div>
                                <div className={styles.tableheader}>Возраст</div>
                                <div className={styles.tableheader}>Пол</div>
                                <div className={styles.tableheader}>Статус</div>
                                <div className={styles.tableheader}>Создан</div>
                                <div className={styles.tableheader}>Изменен</div>
                                <div className={styles.tableheader}></div>
                            </div>
                            <div className={styles.subjecttable}>
                                <div className={styles.wrapper}>
                                    {user.map(item => (
                                        <Row user={item} value={item.status}/>
                                    ))}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {isOpen && <Modal setIsOpen={setIsOpen} />}
        </div>
    );
}

export default ListPage;