import './App.css';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import AuthPage from './Auth/AuthPage';
import RatingPage from './Rating/RatingPage';
import GamePage from './Game/GamePage';
import ActivePage from './Active/ActivePage';
import HistoryPage from './History/HistoryPage';
import ListPage from './List/ListPage';

function App() {
    document.title = "Крестики-нолики";
    return (
          <BrowserRouter>
              <Routes>
                  <Route path="/" element={<AuthPage />} />
                  <Route path="/rating" element={<RatingPage />} />
                  <Route path="/gameField" element={<GamePage />} />
                  <Route path="/active" element={<ActivePage />} />
                  <Route path="/history" element={<HistoryPage />} />
                  <Route path="/list" element={<ListPage />} />
              </Routes>
          </BrowserRouter>


    );
}

export default App;
