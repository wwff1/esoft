import Header from "../Components/Header";
import React from "react";
import styles from "./gamefield.module.css";
import Table from "../Components/Table";

function GamePage() {
    return (
            <div>
                <Header/>
                <link rel="preconnect" href="https://fonts.googleapis.com" />
                <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
                <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet" />
                <div className={styles.maincontainer}>
                    <div className={styles.subjectlist}>
                        <h1>Игроки</h1>
                        <div className={styles.container}>
                            <div className={styles.subjectcontainer}>
                                <div><img className={styles.subjecticon}
                                          src="./zero.svg"/></div>
                                <div className={styles.subjectinfo}>
                                    <div className={styles.subjectname}>Пупкин Владелен Игоревич</div>
                                    <div className={styles.subjectpercent}>63% побед</div>
                                </div>
                            </div>
                            <div className={styles.subjectcontainer}>
                                <div><img className={styles.subjecticon}
                                          src="./x.svg"/></div>
                                <div className={styles.subjectinfo}>
                                    <div className={styles.subjectname}>Плюшкина Екатерина Викторовна</div>
                                    <div className={styles.subjectpercent}>23% побед</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/*<div className={styles.gamecontainer}>*/}
                    {/*    <div className={styles.gametime}>05:12</div>*/}
                    {/*    <div className={styles.gameboard}>*/}
                            <Table />
                    {/*        /!*<div className={styles.cell} ></div>*!/*/}
                    {/*        /!*<div className={styles.cell}></div>*!/*/}
                    {/*        /!*<div className={styles.cell}></div>*!/*/}
                    {/*        /!*<div className={styles.cell}></div>*!/*/}
                    {/*        /!*<div className={styles.cell}></div>*!/*/}
                    {/*        /!*<div className={styles.cell}></div>*!/*/}
                    {/*        /!*<div className={styles.cell}></div>*!/*/}
                    {/*        /!*<div className={styles.cell}></div>*!/*/}
                    {/*        /!*<div className={styles.cell}></div>*!/*/}
                    {/*    </div>*/}
                    {/*    /!*<div className={styles.gamestep}>Ходит&nbsp;<img src="./x.svg"/>&nbsp;Плюшкина Екатерина</div>*!/*/}
                    {/*</div>*/}
                    <div className={styles.chatcontainer}>
                        <div className={styles.msgscontainer}>
                            <div className={styles.other}>
                                <div className={styles.msgheader}>
                                    <div className={styles.x}>Плюшкина Екатерина</div>
                                    <div className={styles.time}>13:40</div>
                                </div>
                                <div className={styles.msgbody}>Ну что, готовься к поражению!!1</div>
                            </div>
                            <div className={styles.me}>
                                <div className={styles.msgheader}>
                                    <div className={styles.zero}>Пупкин Владлен</div>
                                    <div className={styles.time}>13:41</div>
                                </div>
                                <div className={styles.msgbody}>Надо было играть за крестики. Розовый — мой не самый счастливый
                                    цвет
                                </div>
                            </div>

                            <div className={styles.me}>
                                <div className={styles.msgheader}>
                                    <div className={styles.zero}>Пупкин Владлен</div>
                                    <div className={styles.time}>13:45</div>
                                </div>
                                <div className={styles.msgbody}>Я туплю...</div>
                            </div>
                            <div className={styles.other}>
                                <div className={styles.msgheader}>
                                    <div className={styles.x}>Плюшкина Екатерина</div>
                                    <div className={styles.time}>13:47</div>
                                </div>
                                <div className={styles.msgbody}>Отойду пока кофе попить, напиши в тг как сходишь</div>
                            </div>
                        </div>
                        <div className={styles.msginteractiveelements}>
                            <textarea placeholder="Сообщение..."></textarea>
                            <button><img src="./send-btn.svg"/></button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }


export default GamePage;