import Header from '../Components/Header';
import React from "react";
import styles from "../History/history.module.css";

function HistoryPage() {
    const user = [
        { player1: "Терещенко У. Р.", player2: "Многогрешный П. В.", win: "player1", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Горбачёв А. Д.", player2: "Многогрешный П. В.", win: "player2", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Константинов В. Н.", player2: "Сасько Ц. А.", win: "player1", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Никифорова Б. А.", player2: "Горбачёв А. Д.", win: "player2", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Кулишенко К. И.", player2: "Вишняков Ч. М.", win: "player2", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Гриневска Д. Б.", player2: "Кудрявцев Э. В.", win: "player2", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Нестеров Х. А.", player2: "Исаева О. А.", win: "player1", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Исаева О. А.", player2: "Кулишенко К. И.", win: "player2", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Коновалова В. В.", player2: "Терещенко У. Р.", win: "player2", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Казаков Х. Е.", player2: "Овчаренко Б. М.", win: "player1", date: "12 февраля 2022", time: "43 мин 13 сек"},
        { player1: "Сасько Ц. А.", player2: "Никифорова Б. А.", win: "player2", date: "12 февраля 2022", time: "43 мин 13 сек"}

    ];
    return (
        <div>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
            <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet" />
            <Header />
            <div className={styles.maincontainer}>
                <div className={styles.tablecontainer}>
                    <div className={styles.subjectheader}>История игр</div>
                    <div iclassName={styles.subjecttable}>
                        <div className={styles.wrapper}>
                            <div className={styles.subjectrow}>
                                <div className={styles.tableheader}>Игроки</div>
                                <div className={styles.tableheader}>Дата</div>
                                <div className={styles.tableheader}>Время игры</div>
                            </div>
                            {user.map(item => (
                                <div className={styles.subjectrow}>
                                    <div className={styles.cell}>
                                        <div className={styles.cell1}>
                                            <img className={styles.subjecticon} src="./zero.svg"/>{item.player1}
                                            {item.win === "player1" &&
                                            <img className={styles.subjecticon} src="./win-logo.svg"/>
                                            }
                                        </div>
                                        <div className={styles.cell2}>против</div>
                                        <div className={styles.cell1}>
                                            <img className={styles.subjecticon} src="./x.svg"/>{item.player2}
                                            {item.win === "player2" &&
                                            <img className={styles.subjecticon} src="./win-logo.svg"/>
                                            }
                                        </div>
                                    </div>
                                    <div className={styles.cell3}>{item.date}</div>
                                    <div className={styles.cell3}>{item.time}</div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>




    );
}

export default HistoryPage;