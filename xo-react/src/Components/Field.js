import React, { Component } from 'react';

function Field(props) {
    return (
        <input type={props.type} name={props.name} placeholder={props.placeholder} className={props.className} id={props.id} onKeyPress={props.onKeyPress} min={props.min}/>
    );
}
export default Field;