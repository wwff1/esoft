import React from "react";
import styles from "./modal.module.css";
import Field from '../Components/Field';

const Modal = ({ setIsOpen }) => {
    return (
        <>

            <div className={styles.darkBG} onClick={() => setIsOpen(false)} />
            <div className={styles.centered}>
                <button onClick={() => setIsOpen(false)} className={styles.modalButton}><img src="./modal-button.svg"/></button>
                <div className={styles.modalHeader}>Добавьте игрока</div>
                <div className={styles.text}>
                    ФИО
                    <Field placeholder="Иванов Иван Иванович" className={styles.subjecttext}/>
                </div>
                <div className={styles.textInput}>
                    <div className={styles.inputFields}>
                        Возраст
                        <Field type="number" placeholder="0" className={styles.numberInput}  min="1"/>
                    </div>
                    <div className={styles.textInput}>
                        <div className={styles.inputFields}>
                            Пол
                            <div className={styles.radioInput}>
                                <input type="radio" id="shipadd1" name="address"/>
                                <label htmlFor="shipadd1">
                                    <img className="radio-image" src="./female-gender.svg"/>
                                </label>
                                <input type="radio" id="shipadd2" name="address"/>
                                <label htmlFor="shipadd2">
                                    <img className="radio-image" src="./male-gender.svg"/>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>
                <button onClick={() => setIsOpen(false)} className={styles.modalNew}>Добавить</button>
            </div>
        </>
    );
};

export default Modal;