import React from "react";
import styles from "../Game/gamefield.module.css";
import styles1 from "./modal.module.css";
import Modal from "react-modal";
// import Modal from '../Components/ModalWinner';


export default class Table extends React.Component  {
    constructor(props) {
        super(props)
        this.mode = 'x'
        this.setIsOpen = false
        this.setIsOverGame = false
        this.state = {
            cell:[
                { id: 0, field: null},
                { id: 1, field: null},
                { id: 2, field: null},
                { id: 3, field: null},
                { id: 4, field: null},
                { id: 5, field: null},
                { id: 6, field: null},
                { id: 7, field: null},
                { id: 8, field: null}
            ]
        }

    }

    getGameFieldStatus () {
        //Диагональ
        if (this.state.cell[0].field === 'x' && this.state.cell[4].field === 'x' && this.state.cell[8].field === 'x' ){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[0].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[8].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[0].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[8].id).classList.add(styles.winnerGreen)
        }
        else if (this.state.cell[2].field === 'x' && this.state.cell[4].field === 'x' && this.state.cell[6].field === 'x'){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[2].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[6].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[2].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[6].id).classList.add(styles.winnerGreen)
        }
        else if (this.state.cell[0].field === 'o' && this.state.cell[4].field === 'o' && this.state.cell[8].field === 'o'){
            this.mode = "Выиграли o";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[0].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[8].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[0].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[8].id).classList.add(styles.winnerPink)
        }
        else if (this.state.cell[2].field === 'o' && this.state.cell[4].field === 'o' && this.state.cell[6].field === 'o'){
            this.mode = "Выиграли o";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[2].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[6].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[2].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[6].id).classList.add(styles.winnerPink)
        }
        // Горизонталь
        if (this.state.cell[0].field === 'x' && this.state.cell[1].field === 'x' && this.state.cell[2].field === 'x'){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[0].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[1].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[2].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[0].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[1].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[2].id).classList.add(styles.winnerGreen)
        }
        else if (this.state.cell[3].field === 'x' && this.state.cell[4].field === 'x' && this.state.cell[5].field === 'x'){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[3].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[5].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[3].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[5].id).classList.add(styles.winnerGreen)
        }
        else if (this.state.cell[6].field === 'x' && this.state.cell[7].field === 'x' && this.state.cell[8].field === 'x'){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[6].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[7].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[8].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[6].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[7].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[8].id).classList.add(styles.winnerGreen)
        }
        if (this.state.cell[0].field === 'o' && this.state.cell[1].field === 'o' && this.state.cell[2].field === 'o'){
            this.mode = "Выиграли o";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[0].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[1].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[2].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[0].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[1].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[2].id).classList.add(styles.winnerPink)

        }
        else if (this.state.cell[3].field === 'o' && this.state.cell[4].field === 'o' && this.state.cell[5].field === 'o'){
            this.mode = "Выиграли o";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[3].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[5].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[3].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[5].id).classList.add(styles.winnerPink)
        }
        else if (this.state.cell[6].field === 'o' && this.state.cell[7].field === 'o' && this.state.cell[8].field === 'o'){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[6].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[7].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[7].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[6].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[7].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[8].id).classList.add(styles.winnerPink)
        }
        //Вертикаль
        if (this.state.cell[0].field === 'x' && this.state.cell[3].field === 'x' && this.state.cell[6].field === 'x'){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[2].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[6].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[2].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[6].id).classList.add(styles.winnerGreen)
        }
        else if (this.state.cell[1].field === 'x' && this.state.cell[4].field === 'x' && this.state.cell[7].field === 'x'){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[1].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[7].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[1].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[7].id).classList.add(styles.winnerGreen)
        }
        else if (this.state.cell[2].field === 'x' && this.state.cell[5].field === 'x' && this.state.cell[8].field === 'x'){
            this.mode = "Выиграли x";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[2].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[5].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[8].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[2].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[5].id).classList.add(styles.winnerGreen)
            document.getElementById(this.state.cell[8].id).classList.add(styles.winnerGreen)
        }
        if (this.state.cell[0].field === 'o' && this.state.cell[3].field === 'o' && this.state.cell[6].field === 'o'){
            this.mode = "Выиграли o";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[0].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[3].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[6].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[0].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[3].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[6].id).classList.add(styles.winnerPink)
        }
        else if (this.state.cell[1].field === 'o' && this.state.cell[4].field === 'o' && this.state.cell[7].field === 'o'){
            this.mode = "Выиграли o";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[1].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[4].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[7].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[1].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[4].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[7].id).classList.add(styles.winnerPink)
        }
        else if (this.state.cell[2].field === 'o' && this.state.cell[5].field === 'o' && this.state.cell[8].field === 'o'){
            this.mode = "Выиграли o";
            this.setIsOverGame = true;
            document.getElementById(this.state.cell[2].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[5].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[8].id).classList.remove(styles.cell)
            document.getElementById(this.state.cell[2].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[5].id).classList.add(styles.winnerPink)
            document.getElementById(this.state.cell[8].id).classList.add(styles.winnerPink)
        }
        if(this.mode === 'x' || this.mode === 'o')
        {
            let check = true;
            for (const [key, value] of Object.entries(this.state.cell)) {
                console.log(value.field)
                if(value.field === null)
                {
                    check = false;
                    break;
                }
            }
            if (check)
            {
                this.mode = 'Ничья';
                this.setIsOverGame = true;
            }
        }
    }
    onClick = () => {
        this.setIsOverGame = false;
        this.setState({setIsOverGame: false})
    }


    onTdClick = (e) => {
        if(this.state.cell[e.currentTarget.id].field == null)
        {
            const img = document.createElement('img')
            let cell = [...this.state.cell];
            let item = {...cell[e.currentTarget.id]};
            if(this.mode === 'x')
            {
                img.src = "./xxl-x.svg"
                img.id = e.currentTarget.id;
                item.field = 'x';
                cell[e.currentTarget.id] = item;
                this.state.cell = cell
                this.setState({mode: 'o'})
                e.target.appendChild(img)
            }
            else
            {
                img.src = "./xxl-zero.svg"
                img.id = e.currentTarget.id;
                item.field = 'o';
                cell[e.currentTarget.id] = item;
                this.state.cell = cell
                this.setState({mode: 'x'})
            }
            e.target.appendChild(img)
            this.mode = this.mode === 'x' ? 'o' : 'x'
            console.log(this.state.cell)
            this.getGameFieldStatus()


        }
    }

    render() {
        return (
                <div className={styles.gamecontainer}>
                    <div className={styles.gametime}>05:12</div>
                    <div className={styles.gameboard}>
                        <table className={styles.gameboard}>
                            <tbody className={styles.gameboard}>
                            {
                                this.state.cell.map((cell, index) => {
                                    const {id} = cell //destructuring
                                    return (
                                        <tr className={styles.cell}>
                                            <td key={id} id={id} className={styles.cell} onClick={this.onTdClick}></td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                    {this.mode === 'x' &&
                        <div className={styles.gamestep}>Ходит&nbsp;<img className={styles.subjecticon} src="./x.svg"/>&nbsp;Плюшкина Екатерина</div>
                    }
                    {this.mode === 'o' &&
                        <div className={styles.gamestep}>Ходит&nbsp;<img className={styles.subjecticon} src="./zero.svg"/>&nbsp;Пупкин Владелен</div>
                    }
                    <Modal className={styles.modal}
                        isOpen={this.setIsOverGame}
                        onRequestClose={() => this.onClick}
                    >
                        <div className={styles.modalWindow}>
                            {this.mode === 'Выиграли x' &&
                                <>
                                    <img className={styles.iconWinner} src="./winner.svg"/>
                                    <div className={styles.headerWinner}>Плюшкина Екатерина победила!</div>
                                </>

                            }
                            {this.mode === 'Выиграли o' &&
                                <>
                                    <img className={styles.iconWinner} src="./winner.svg"/>
                                    <div className={styles.headerWinner}>Пупкин Владелен победил!</div>
                                </>
                            }
                            {this.mode === 'Ничья' &&
                            <div className={styles.headerWinner}>Ничья</div>
                            }
                            <button onClick={this.onClick} className={styles.modalNew}>Новая игра</button>
                            <button onClick={this.onClick}className={styles.modalMenu}>Выйти в меню</button>
                        </div>

                    </Modal>
            </div>
        )
    }
}