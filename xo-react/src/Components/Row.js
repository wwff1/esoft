import React from "react";
import styles from "../List/list.module.css";

export default class Row extends React.Component  {
    constructor(props) {
        super(props);
        this.user = this.props.user;
        this.state = {isToggleOn: this.props.value};
        this.var = <span>Разблокировать</span>
        this.style1 = <div className={styles.cell3}>
                        <img className={styles.subjecticon1} src="./block.svg" />Заблокировать
                    </div>;
        this.style2 = 'Разблокировать';
        this.text1 = 'Активен'
        this.text2 = 'Заблокирован';
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(prevState => ({
            isToggleOn: !prevState.isToggleOn
        }));
    }

    render() {
        return (
            <div className={styles.subjectrow}>
                <div className={styles.cell1}>{this.user.name}</div>
                <div className={styles.cell1}>{this.user.age}</div>
                <div className={styles.cell1}>
                    {this.user.gender ? <img className={styles.subjecticon} src="./male-gender.svg"/> : <img className={styles.subjecticon} src="./female-gender.svg"/> }
                </div>
                <div className={this.state.isToggleOn ? styles.cell2 : styles.cell2Block}>
                    {this.state.isToggleOn ? this.text1 : this.text2 }
                </div>
                <div className={styles.cell1}>{this.user.create}</div>
                <div className={styles.cell1}>{this.user.change}</div>
                <div onClick={this.handleClick} className={styles.cell3}>
                    {this.state.isToggleOn ? this.style1 : this.style2 }
                </div>


            </div>
        );
    }
}