import React, { Component } from 'react';
import {NavLink} from "react-router-dom";
import "../Components/header.css";

const setActive = ({ isActive }) =>(isActive ? " active" : "");
function Header() {
    return (
    <header>
        <title>My Portfolio</title>
        <div id="logo"><img src="./s-logo.svg"/></div>
        <div id="nav-panel">
            <NavLink to="/gameField" className={setActive}>Игровое поле</NavLink>
            <NavLink to="/rating" className={setActive}>Рейтинг</NavLink>
            <NavLink to="/active" className={setActive}>Активные игроки</NavLink>
            <NavLink to="/history" className={setActive}>История игр</NavLink>
            <NavLink to="/list" className={setActive}>Список игроков</NavLink>
        </div>
        <button><img src="./signout-icon.svg"/></button>
    </header>
    );
}
export default Header;