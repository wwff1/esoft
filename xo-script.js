const gameField = [
    ['x', 'o', null],
    ['x', null, 'o'],
    ['x', 'o', 'o']
    ];

 let result = '';

function checkDiagonal (gameField) {
    if (gameField[0][0] === 'x' &&
      gameField[1][1] === 'x' &&
      gameField[2][2] === 'x')
        result = "Выиграли x";
    else if (gameField[0][2] === 'x' &&
    gameField[1][1] === 'x' &&
    gameField[2][0] === 'x')
        result = "Выиграли x";
    else if(gameField[0][0] === 'o' &&
      gameField[1][1] === 'o' &&
      gameField[2][2] === 'o')
        result = "Выиграли o";
    else if (gameField[0][2] === 'o' &&
    gameField[1][1] === 'o' &&
    gameField[2][0] === 'o')
        result = "Выиграли o";
}

function checkHorizontal (gameField) {
    if (gameField[0][0] === 'x' &&
      gameField[0][1] === 'x' &&
      gameField[0][2] === 'x')
        result = "Выиграли x";
    else if (gameField[1][0] === 'x' &&
    gameField[1][1] === 'x' &&
    gameField[1][2] === 'x')
        result = "Выиграли x";
    else if (gameField[2][0] === 'x' &&
    gameField[2][1] === 'x' &&
    gameField[2][2] === 'x')
        result = "Выиграли x";
    else if(gameField[0][0] === 'o' &&
      gameField[0][1] === 'o' &&
      gameField[0][2] === 'o')
        result = "Выиграли o";
    else if (gameField[1][0] === 'o' &&
    gameField[1][1] === 'o' &&
    gameField[1][2] === 'o')
        result = "Выиграли o";
    else if (gameField[2][0] === 'o' &&
    gameField[2][1] === 'o' &&
    gameField[2][2] === 'o')
        result = "Выиграли o";
}

function checkVertical (gameField) {
    if (gameField[0][0] === 'x' &&
      gameField[1][0] === 'x' &&
      gameField[2][0] === 'x')
        result = "Выиграли x";
    else if (gameField[0][1] === 'x' &&
    gameField[1][1] === 'x' &&
    gameField[2][1] === 'x')
        result = "Выиграли x";
    else if (gameField[0][2] === 'x' &&
    gameField[1][2] === 'x' &&
    gameField[2][2] === 'x')
        result = "Выиграли x";
    else if(gameField[0][0] === 'o' &&
      gameField[1][0] === 'o' &&
      gameField[2][0] === 'o')
        result = "Выиграли o";
    else if (gameField[0][1] === 'o' &&
    gameField[1][1] === 'o' &&
    gameField[2][1] === 'o')
        result = "Выиграли o";
    else if (gameField[0][2] === 'o' &&
    gameField[1][2] === 'o' &&
    gameField[2][2] === 'o')
        result = "Выиграли o";
}

checkDiagonal(gameField);
checkHorizontal(gameField);
checkVertical(gameField);

if(result === '')
{
    result = 'Ничья';
    alert(result);
}
else alert(result);
