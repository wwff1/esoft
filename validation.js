const sendForm = (e) => {
    console.log('Логин: ', document.querySelector('#login').value)
    console.log('Пароль: ', document.querySelector('#password').value)
};

const pressOnInput = (e) => {
    if (e.charCode == 46 || e.charCode == 95 || (e.charCode >= 65 && e.charCode <= 90)
        || (e.charCode >= 97 && e.charCode <= 122) || (e.charCode >= 48 && e.charCode <= 57))
    	return true;
    else {
         e.preventDefault();
    }
};

document.querySelector('#auth-container').addEventListener('submit', sendForm);
document.querySelector('#auth-interactive-elements').addEventListener('keypress', pressOnInput);


