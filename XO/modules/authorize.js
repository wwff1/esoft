const bcrypt = require('bcryptjs');

class authorize {
    async login(req, res) {
        try {

            const fs = require('fs');
            let user = JSON.parse(fs.readFileSync('./modules/data.json'))
            console.log(user)
            console.log(req.body)
            const {login, password} = req.body
            if (login !== user.login) {
                return res.status(400).json({message: `Пользователь ${login} не найден`})
            }
            const validPassword = bcrypt.compareSync(password, user.password)
            if (!validPassword) {
                return res.status(400).json({message: `Введен неверный пароль`})
            }
            res.redirect(301, '/rating')
            return res.status(200)
        } catch (e) {
            console.log(e)
            res.status(400).json({message: 'Login error'})
        }
    }
}
module.exports = new authorize();