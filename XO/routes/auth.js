const express = require('express');
const router = express.Router();
const authorize = require('../modules/authorize')

router.get('/', function(req, res, next) {
  res.render('auth', { title: 'Авторизация' });
});

router.post('/',  authorize.login)

module.exports = router;
