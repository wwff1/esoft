var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  var mascots = [
    { name: 'Александров Игнат Анатолиевич', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Шевченко Рафаил Михайлович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Мазайло Трофим Артёмович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Логинов Остин Данилович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Борисов Йошка Васильевич', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Соловьёв Ждан Михайлович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Негода Михаил Эдуардович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Гордеев Шамиль Леонидович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Волков Эрик Алексеевич', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Кузьмин Ростислав Васильевич', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Стрелков Филипп Борисович', game: "24534", win: 9824, lose: 1222, winrate: '87%'},
    { name: 'Галкин Феликс Платонович', game: "24534", win: 9824, lose: 1222, winrate: '87%'}
  ];
  res.render('rating', { title: 'Авторизация', mascots: mascots });
});


module.exports = router;