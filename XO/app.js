var express = require('express');
var path = require('path');

var authRouter = require('./routes/auth');
var ratingRouter = require('./routes/rating');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', authRouter);
app.use('/rating', ratingRouter);

module.exports = app;
