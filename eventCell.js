let mode = 'x';
const cell = document.getElementsByClassName('cell');

for (var i = 0; i < cell.length; i++) {
    cell[i].addEventListener('click', clickCell);
}

function clickCell(e) {
    const img = document.createElement('img');
    img.src = "../imgs/xxl-x.svg";
    if (mode === 'x')
    {   
        mode = 'o';
        document.getElementById('game-step').innerHTML = 'Ходит&nbsp;<img src="../imgs/zero.svg"/>&nbsp;Владелен Пупкин'
    }
    else {
        img.src = "../imgs/xxl-zero.svg";
        document.getElementById('game-step').innerHTML = 'Ходит&nbsp;<img src="../imgs/x.svg"/>&nbsp;Плюшкина Екатерина'
        mode = 'x';
    }
    e.target.appendChild(img);
    e.srcElement.removeEventListener('click', clickCell);
    document.getElementsByClassName(e.target).removeEventListener('click', clickCell);
};
